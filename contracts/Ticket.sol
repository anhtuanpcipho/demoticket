// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Ticket is Ownable {
    // variables and mappings
    address public tokenAddress;
    uint256 public maximumPlayers = 100;
    uint256 totalPlayer = 0;
    uint256 public luckyNumber = 0;
    bool finishGame = false;
    uint256 bettingPrice = 10 ether;
    uint256 public totalBettingBid = 0;
    address[] public players;
    uint256 public rewardAmount = 0;

    mapping(address => uint256) public _bettingNumber;
    mapping(address => bool) public _betted;
    mapping(address => bool) public _winners;

    // functions
    constructor(address _tokenAddress) {
        tokenAddress = _tokenAddress;
    }

    function changePrice(uint256 _bettingPrice) external onlyOwner {
        bettingPrice = _bettingPrice;
    }

    // this function for test only
    function changeMaximumPlayers(uint256 _maximumPlayers) external onlyOwner {
        maximumPlayers = _maximumPlayers;
    }

    function betting(uint256 _number) public {
        require(_number >=0 && _number <= 99, "invalid betting number");
        require(totalPlayer < maximumPlayers, "exceed maximum number");
        require(finishGame == false, "already finished game");
        require(!_betted[msg.sender], "already betted");
        require(owner() != msg.sender, "dealer cannot join");
        IERC20 token = IERC20(tokenAddress);
        token.transferFrom(msg.sender, address(this), bettingPrice);
        _bettingNumber[msg.sender] = _number;
        maximumPlayers++;
        totalBettingBid += bettingPrice;
        players.push(msg.sender);
        _betted[msg.sender] = true;
    }

    function terminate() external onlyOwner {
        IERC20 token = IERC20(tokenAddress);
        luckyNumber = block.number % 100;
        finishGame = true;
        uint256 winningPlayers = 0;
        for (uint256 i = 0; i < players.length; i++) {
            if (_bettingNumber[players[i]] == luckyNumber) {
                _winners[players[i]] = true;
                winningPlayers++;
            }
        }

        if (winningPlayers == 0) {
            token.transfer(msg.sender, totalBettingBid);
        } else {
            token.transfer(msg.sender, totalBettingBid / 10);
            rewardAmount = (totalBettingBid * 9) / 10 / winningPlayers;
        }
    }

    function withdrawReward() public {
        require(finishGame, "this game have not finished");
        require(_winners[msg.sender], "you must win this game");
        IERC20 token = IERC20(tokenAddress);
        token.transfer(msg.sender, rewardAmount);
    }

    // this function for test only
    function getBlockNumber() public view onlyOwner returns (uint256) {
        return block.number;
    }

    function getPlayers() public view returns(address[] memory) {
        return players;
    }
}
