import { Ticket } from "./../typechain-types/contracts/Ticket";
import { Token } from "./../typechain-types/contracts/Token";
import { expect } from "chai";
import { ethers } from "hardhat";

describe("Start playing Ticket Game", () => {
  let deployer, alice, bob, daniel: any;
  let token: Token;
  let ticket: Ticket;
  beforeEach(async () => {
    [deployer, alice, bob, daniel] = await ethers.getSigners();
    token = (await (
      await (await ethers.getContractFactory("Token")).deploy()
    ).deployed()) as Token;
    ticket = (await (
      await (await ethers.getContractFactory("Ticket")).deploy(token.address)
    ).deployed()) as Ticket;
    await token.mint(ethers.utils.parseEther("1000"), alice.address);
    await token.mint(ethers.utils.parseEther("1000"), bob.address);
    await token.mint(ethers.utils.parseEther("1000"), daniel.address);
  });

  it("Start the game", async () => {
    // alice, bob and daniel start betting
    await token
      .connect(alice)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(alice).betting("01");
    await token
      .connect(bob)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(bob).betting("02");
    await token
      .connect(daniel)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(daniel).betting("03");
    const contractBalance = await token.balanceOf(ticket.address);
    // anyone else can check the balance
    expect(contractBalance).to.equal(ethers.utils.parseEther("30"));
  });

  it("Too much players", async () => {
    [await ticket.changeMaximumPlayers(1)];
    await token
      .connect(alice)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(alice).betting("01");
    await token
      .connect(bob)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    expect(ticket.connect(bob).betting("02")).to.revertedWith(
      "exceed maximum number"
    );
  });

  it("dealer cannot be a player", async () => {
    await token.approve(ticket.address, ethers.utils.parseEther("10"));
    expect(ticket.betting("02")).to.revertedWith("dealer cannot join");
  });

  it("Cannot betting after finishing the game", async () => {
    await ticket.terminate();
    await token
      .connect(alice)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    expect(ticket.connect(bob).betting("02")).to.revertedWith(
      "already finished game"
    );
  });

  it("Invalid betting number", async () => {
    await token
      .connect(alice)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    expect(ticket.connect(bob).betting("103")).to.revertedWith(
      "invalid betting number"
    );
  });

  it("cannot betting two times", async () => {
    await token
      .connect(alice)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(alice).betting("01");
    await token
      .connect(alice)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    expect(ticket.connect(alice).betting("02")).to.revertedWith(
      "already betted"
    );
  });

  it("stop the game and dealer receive 10% reward", async () => {
    await token
      .connect(alice)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(alice).betting("66");
    await token
      .connect(bob)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(bob).betting("66");
    await token
      .connect(daniel)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(daniel).betting("67");
    const contractBalance = await token.balanceOf(ticket.address);
    expect(contractBalance).to.equal(ethers.utils.parseEther("30"));
    const totalBettingBid = await ticket.totalBettingBid();
    console.log(totalBettingBid, "total betting bid");
    await ticket.terminate();
    const luckyNumber = await ticket.luckyNumber();
    console.log("lucky number is ", luckyNumber);
    const dealerBalance = await token.balanceOf(deployer.address);
    // 100000 token default and 3 token is 10% of 30 token betting bid in game
    expect(dealerBalance).to.equal(ethers.utils.parseEther("100003"));

    // rewards will be shared for common winners
    await ticket.connect(alice).withdrawReward();
    await ticket.connect(bob).withdrawReward();
    const aliceBalance = await token.balanceOf(alice.address);
    const bobBalance = await token.balanceOf(bob.address);

    const rewardAmount = await ticket.rewardAmount();
    expect(rewardAmount).to.equal(ethers.utils.parseEther("13.5"));

    const aliceBool = await ticket._winners(daniel.address);
    console.log(aliceBool);

    const players = await ticket.getPlayers();
    console.log("players", players[0]);

    // must transfer withdraw reward for user to decrease gas comsumption
    // alice will receive 13.5 token again cause only alice and bob win this game
    expect(aliceBalance).to.equal(ethers.utils.parseEther("1003.5"));
    // similar to bob
    expect(bobBalance).to.equal(ethers.utils.parseEther("1003.5"));
    // but daniel cannot withdraw reward as fail this game
    expect(ticket.connect(daniel).withdrawReward()).to.revertedWith(
      "you must win this game"
    );
  });

  it("dealer can receive all the betting token if no one win the game", async () => {
    await token
      .connect(alice)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(alice).betting("66");
    await token
      .connect(bob)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(bob).betting("66");
    await token
      .connect(daniel)
      .approve(ticket.address, ethers.utils.parseEther("10"));
    await ticket.connect(daniel).betting("67");
    const contractBalance = await token.balanceOf(ticket.address);
    expect(contractBalance).to.equal(ethers.utils.parseEther("30"));
    const totalBettingBid = await ticket.totalBettingBid();
    console.log(totalBettingBid, "total betting bid");
    await ticket.terminate();
    const luckyNumber = await ticket.luckyNumber();
    console.log("lucky number is ", luckyNumber);
    const dealerBalance = await token.balanceOf(deployer.address);
    // 100000 token default and 30 token as no one win game
    expect(dealerBalance).to.equal(ethers.utils.parseEther("100030"));
  });
});
