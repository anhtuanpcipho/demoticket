import { Token } from "./../typechain-types/contracts/Token";
import { expect } from "chai";
import { ethers } from "hardhat";

describe("Demo Token", function () {
  let deployer, alice, bob, daniel: any;
  let token: Token;
  beforeEach(async () => {
    [deployer, alice, bob, daniel] = await ethers.getSigners();
    token = (await (
      await (await ethers.getContractFactory("Token")).deploy()
    ).deployed()) as Token;
  });

  it("Test Demo Token", async () => {
    const balance = await token.balanceOf(deployer.address);
    console.log(`balance of deployer is ${balance} Wei`);
    expect(balance).to.equal(ethers.utils.parseEther("100000"));
  });

  it("Transfer Token to Alice", async () => {
    await token.transfer(alice.address, ethers.utils.parseEther("10"));

    const aliceBalance = await token.balanceOf(alice.address);
    expect(aliceBalance).to.equal(ethers.utils.parseEther("10"));
  });
});
